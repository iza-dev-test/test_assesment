# Problem:
We have an undirected and weighted graph of n nodes (indexed from 0 to n-1). Each edge between two nodes has an associated success probability. The objective is to find the path with the maximum success probability to go from a given start node to an end node.

# Solution:
To solve this problem, I used a variant of Dijkstra's algorithm, adapted to maximize probabilities rather than minimize distances.

# Key Steps:

1. **Graph Representation**:
The graph is represented by an adjacency list where each node has a list of its neighbors and the corresponding success probabilities.

2. **Using a Max Heap**:
A max heap is used to always explore the path with the highest success probability first.
Path probabilities are calculated by multiplying the probabilities of the edges along the path.

3. **Probability Propagation**:
The starting node's probability is initialized to 1 (100%) and the other nodes' probabilities to 0.
For each explored node, the probabilities of its neighbors are updated if a higher probability path is found.

4. **Stopping Criteria**:
The algorithm stops when it reaches the end node or when the heap is empty.
If no path exists, the function returns 0.

5. **Visualization**:
To better understand and verify the graph, I also used Three.js to visualize the graph and the possible paths.

# Conclusion:
Methodically, I aimed to efficiently find the path with the maximum success probability in a weighted graph by using an appropriate data structure to optimize the calculations.

# To run the project:
1. Install Live Server from VSC
2. Right-click on index.html and select 'Open with Live Server'.