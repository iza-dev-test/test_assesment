class MaxHeap {
    constructor() {
        this.heap = [];
    }

    push(val) {
        this.heap.push(val);
        this._bubbleUp(this.heap.length - 1);
    }

    pop() {
        if (this.heap.length === 1) return this.heap.pop();
        const max = this.heap[0];
        this.heap[0] = this.heap.pop();
        this._bubbleDown(0);
        return max;
    }

    empty() {
        return this.heap.length === 0;
    }

    _bubbleUp(index) {
        while (index > 0) {
            const parentIndex = Math.floor((index - 1) / 2);
            if (this.heap[parentIndex][0] >= this.heap[index][0]) break;
            [this.heap[parentIndex], this.heap[index]] = [this.heap[index], this.heap[parentIndex]];
            index = parentIndex;
        }
    }

    _bubbleDown(index) {
        const length = this.heap.length;
        while (true) {
            const leftChild = 2 * index + 1;
            const rightChild = 2 * index + 2;
            let largest = index;

            if (leftChild < length && this.heap[leftChild][0] > this.heap[largest][0]) {
                largest = leftChild;
            }

            if (rightChild < length && this.heap[rightChild][0] > this.heap[largest][0]) {
                largest = rightChild;
            }

            if (largest === index) break;
            [this.heap[index], this.heap[largest]] = [this.heap[largest], this.heap[index]];
            index = largest;
        }
    }
}

function createGraph(edges, succProb) {
    let graph = new Map();
    for (let i = 0; i < edges.length; i++) {
        let [u, v] = edges[i];
        if (!graph.has(u)) graph.set(u, []);
        if (!graph.has(v)) graph.set(v, []);
        graph.get(u).push([v, succProb[i]]);
        graph.get(v).push([u, succProb[i]]);
    }
    return graph;
}

function maxProbability(n, edges, succProb, start, end) {
    let graph = createGraph(edges, succProb);
    let maxHeap = new MaxHeap();
    maxHeap.push([1.0, start]);
    let probabilities = new Array(n).fill(0.0);
    probabilities[start] = 1.0;

    while (!maxHeap.empty()) {
        let [prob, node] = maxHeap.pop();

        if (node === end) {
            return { prob, probabilities, graph };
        }

        if (graph.has(node)) {
            for (let [neighbor, edgeProb] of graph.get(node)) {
                let newProb = prob * edgeProb;
                if (newProb > probabilities[neighbor]) {
                    probabilities[neighbor] = newProb;
                    maxHeap.push([newProb, neighbor]);
                }
            }
        }
    }
    return { prob: 0.0, probabilities, graph };
}

function visualizeGraph(n, graph, probabilities) {
    let scene = new THREE.Scene();
    scene.background = new THREE.Color(0xEA9999);
    let camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    let renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // Nodes positions
    let nodes = new Map();
    let index = 0;
    for (let node of graph.keys()) {
        let position = new THREE.Vector3(Math.random() * 2 - 1, Math.random() * 2 - 1, 0);
        nodes.set(node, { position, index });
        index++;
    }

    // Create edges as planes
    let vertices = [];
    for (let node of graph.keys()) {
        vertices.push(nodes.get(node).position);
    }

    for (let i = 0; i < vertices.length; i++) {
        let start = vertices[i];
        let end = vertices[(i + 1) % vertices.length];
        let edgeLength = start.distanceTo(end);

        let edgeGeometry = new THREE.PlaneGeometry(edgeLength, 0.1);
        let edgeMaterial = new THREE.MeshBasicMaterial({ color: 0x0000ff, side: THREE.DoubleSide });
        let edgePlane = new THREE.Mesh(edgeGeometry, edgeMaterial);

        let midPoint = new THREE.Vector3().addVectors(start, end).multiplyScalar(0.5);
        edgePlane.position.set(midPoint.x, midPoint.y, midPoint.z);

        let angle = Math.atan2(end.y - start.y, end.x - start.x);
        edgePlane.rotation.z = angle;

        scene.add(edgePlane);
    }

    // Create hinges as pairs of planes
    for (let i = 0; i < vertices.length; i++) {
        let current = vertices[i];
        let next = vertices[(i + 1) % vertices.length];
        let prev = vertices[(i - 1 + vertices.length) % vertices.length];

        let prevDir = new THREE.Vector3().subVectors(current, prev).normalize();
        let nextDir = new THREE.Vector3().subVectors(next, current).normalize();

        let anglePrev = Math.atan2(prevDir.y, prevDir.x);
        let angleNext = Math.atan2(nextDir.y, nextDir.x);

        let hingeWidth = 0.1; 
        let hingeHeight = 0.2; 
        let hingeGeometry = new THREE.PlaneGeometry(hingeWidth, hingeHeight);
        let hingeMaterial = new THREE.MeshBasicMaterial({ color: 0xffff00, side: THREE.DoubleSide });

        let hingePlane1 = new THREE.Mesh(hingeGeometry, hingeMaterial);
        hingePlane1.position.set(current.x, current.y, current.z);
        hingePlane1.rotation.z = anglePrev + Math.PI / 2; 

        let hingePlane2 = new THREE.Mesh(hingeGeometry, hingeMaterial);
        hingePlane2.position.set(current.x, current.y, current.z);
        hingePlane2.rotation.z = angleNext + Math.PI / 2; 

        scene.add(hingePlane1);
        scene.add(hingePlane2);
    }

    camera.position.z = 2;

    function animate() {
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
    }
    animate();
}

function runExample(n, edges, succProb, start, end) {
    let constraintError = checkConstraints(n, edges, succProb, start, end);
    if (constraintError) {
        console.error(constraintError);
        return;
    }
    else {
        console.log("All good for constraints");
    }

    let { prob, probabilities, graph } = maxProbability(n, edges, succProb, start, end);
    console.log(`Maximum probability from ${start} to ${end}: ${prob}`);
    visualizeGraph(n, graph, probabilities);
}

// Example 1
runExample(3, [[0, 1], [1, 2], [0, 2]], [0.5, 0.5, 0.2], 0, 2);

// Example 2
// runExample(3, [[0, 1], [1, 2], [0, 2]], [0.5, 0.5, 0.3], 0, 2);

// Example 3
// runExample(3, [[0, 1]], [0.5], 0, 2);

function checkConstraints(n, edges, succProb, start, end) {
    if (n < 2 || n > 10000) {
        return "Constraint violated: 2 <= n <= 10^4";
    }
    if (start < 0 || start >= n || end < 0 || end >= n) {
        return "Constraint violated: 0 <= start, end < n";
    }
    if (start === end) {
        return "Constraint violated: start != end";
    }
    if (edges.length !== succProb.length || edges.length > 20000) {
        return "Constraint violated: 0 <= succProb.length == edges.length <= 2*10^4";
    }
    for (let i = 0; i < edges.length; i++) {
        let [a, b] = edges[i];
        if (a < 0 || a >= n || b < 0 || b >= n || a === b) {
            return "Constraint violated: 0 <= a, b < n and a != b";
        }
        if (succProb[i] < 0 || succProb[i] > 1) {
            return "Constraint violated: 0 <= succProb[i] <= 1";
        }
    }
    return null;
}